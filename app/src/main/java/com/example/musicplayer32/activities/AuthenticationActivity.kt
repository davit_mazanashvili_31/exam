package com.example.musicplayer32.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.musicplayer32.R
import com.google.firebase.auth.FirebaseAuth

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (FirebaseAuth.getInstance().currentUser != null){
            goToProfile()
        }
        setContentView(R.layout.activity_main)
        val controller = findNavController(R.id.nav_host_fragment)

        val fragmentSet = setOf<Int>(
                    R.id.loginFragment,
                    R.id.forgotPasswordFragment,
                    R.id.registrationFragment
        )


        setupActionBarWithNavController(controller, AppBarConfiguration(fragmentSet))

    }
    private fun goToProfile(){
        startActivity(Intent(this, MusicPlayer::class.java))
        finish()
    }
}