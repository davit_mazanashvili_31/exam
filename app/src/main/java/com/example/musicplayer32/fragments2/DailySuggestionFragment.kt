package com.example.musicplayer32.fragments2

import kotlin.random.Random
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.musicplayer32.R
import com.google.firebase.database.FirebaseDatabase


class DailySuggestionFragment : Fragment(R.layout.suggestions_layout) {
    private lateinit var genre: Spinner
    private lateinit var firstSongName: TextView
    private lateinit var firstSongAuthor: TextView
    private lateinit var secondSongName: TextView
    private lateinit var secondSongAuthor: TextView
    private lateinit var thirdSongName: TextView
    private lateinit var thirdSongAuthor: TextView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        genre = view.findViewById(R.id.music_genres) as Spinner
        firstSongName = view.findViewById(R.id.firstSongName)
        firstSongAuthor = view.findViewById(R.id.firstSongAuthor)
        secondSongName = view.findViewById(R.id.secondSongName)
        secondSongAuthor = view.findViewById(R.id.secondSongAuthor)
        thirdSongName = view.findViewById(R.id.thirdSongName)
        thirdSongAuthor = view.findViewById(R.id.thirdSongAuthor)

        val genres = arrayOf("Rock", "Rap", "Metal", "Classical", "Pop")

        genre.adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_expandable_list_item_1,
            genres
        )

        genre.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val choice = genres.get(position)
                val db = FirebaseDatabase.getInstance().getReference(choice)
                var numList = mutableListOf<Int>()
                var i = 1
                while (i != 4){
                    var number = rand(1,10)
                    if (number !in numList){
                        numList.add(number)
                        var path = "song" + number.toString()
                        if (i == 1){
                            db.child(path).get().addOnSuccessListener {
                                if (it.exists()) {
                                    firstSongName.text = it.child("title").value.toString()
                                    firstSongAuthor.text = it.child("author").value.toString()
                                }
                            }
                            i += 1
                        }else if( i == 2){
                            db.child(path).get().addOnSuccessListener {
                                if (it.exists()) {
                                    secondSongName.text = it.child("title").value.toString()
                                    secondSongAuthor.text = it.child("author").value.toString()
                                }
                            }
                            i += 1
                        }else{
                            db.child(path).get().addOnSuccessListener {
                                if (it.exists()) {
                                    thirdSongName.text = it.child("title").value.toString()
                                    thirdSongAuthor.text = it.child("author").value.toString()
                                }
                            }
                            i += 1
                        }

                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }


        }
    }

    fun rand(start: Int, end: Int): Int {
        require(start <= end) { "Illegal Argument" }
        return Random(System.nanoTime()).nextInt(start, end + 1)
    }
}
