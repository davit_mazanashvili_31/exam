package com.example.musicplayer32.fragments
import android.content.Intent
import android.os.Bundle


import android.view.View
import android.widget.Button
import android.widget.EditText

import android.widget.TextView
import android.widget.Toast

import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.musicplayer32.activities.MusicPlayer
import com.example.musicplayer32.R
import com.google.firebase.auth.FirebaseAuth

class loginFragment : Fragment(R.layout.login_layout) {
    private lateinit var editTextEmail : EditText
    private lateinit var editTextPassword : EditText
    private lateinit var buttonLogin : Button
    private lateinit var textForgotPassword : TextView
    private lateinit var textRegister : TextView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        buttonLogin = view.findViewById(R.id.buttonLogin)
        textForgotPassword = view.findViewById(R.id.textForgetPassword)
        textRegister = view.findViewById(R.id.textRegister)
        val controller = Navigation.findNavController(view)
        buttonLogin.setOnClickListener{
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            if ( email.isEmpty() || password.isEmpty()){
                Toast.makeText(requireContext(), "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener{task ->
                if (task.isSuccessful){
                    goToMusicPlayer()
                }
            }
        }
        textRegister.setOnClickListener{
            val action = loginFragmentDirections.actionLoginFragmentToRegistrationFragment()
            controller.navigate(action)
        }
        textForgotPassword.setOnClickListener{
            val action = loginFragmentDirections.actionLoginFragmentToForgotPasswordFragment()
            controller.navigate(action)
        }

    }
    private fun goToMusicPlayer(){
        startActivity(Intent(requireContext(), MusicPlayer::class.java))
        requireActivity().finish()

    }


}