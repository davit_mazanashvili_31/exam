package com.example.musicplayer32.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.musicplayer32.R
import com.google.firebase.auth.FirebaseAuth

class RegistrationFragment : Fragment(R.layout.registration_layout) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val buttonSignUp = view.findViewById<Button>(R.id.buttonSignUp)
        val editTextEmail = view.findViewById<EditText>(R.id.editTextEmail)
        val editTextPassword = view.findViewById<EditText>(R.id.editTextPassword)


        buttonSignUp.setOnClickListener {
            val controller = Navigation.findNavController(view)
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            if (email.isEmpty() || password.isEmpty()){
                Toast.makeText(requireContext(), "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener{ task ->
                if (task.isSuccessful){
                    val action = RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
                    controller.navigate(action)
                }else{
                    Toast.makeText(requireContext(), "Error!", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
}