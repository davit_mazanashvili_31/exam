package com.example.musicplayer32.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.musicplayer32.R
import com.google.firebase.auth.FirebaseAuth

class ForgotPasswordFragment : Fragment(R.layout.forget_password_layout) {
    private lateinit var buttonSend : Button
    private lateinit var editTextEmail : EditText
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonSend = view.findViewById(R.id.buttonSend)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        val controller = Navigation.findNavController(view)
        buttonSend.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextEmail.text.toString()
            if (email.isEmpty() || password.isEmpty()){
                Toast.makeText(requireContext(), "Error!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
            val action = ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToLoginFragment()
            controller.navigate(action)
        }
    }
}