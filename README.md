Our application is a decent music player, with a very simple interface.
It scans smartphone's external storage and shows every audio file in recycler view.
but the most important thing is that we created a second fragment, which anyone can navigate
throught menu and explore new melodies. We use database to update music suggestions daily. 
Users can choose whichever music genre or genres they like the most and we'll give them 10 different
songs,that we think they'll enjoy listening to. The only thing our user needs to do, is to register with
email and password and leave the rest to us.
